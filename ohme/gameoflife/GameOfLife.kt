import java.util.Arrays

class GameOfLife {
    fun makeArray(cols: Int, rows: Int) {
        // println("cols: ${cols} and rows: ${rows} and cols.class: ${cols::class.simpleName}")

        // val list = 1..cols
        // println("list: ${list::class.simpleName}")

        // for (i in list) { 
        //     println("i from list: ${i}")
        //     // list[i] = listOf(rows)
        // }

        // val colArray: IntArray = (1..cols).toList().toIntArray()
        var result = arrayOf<Int>()
        
        println("""
        (1..cols).toList(): ${(1..cols).toList()}
        (1..cols).toList().toTypedArray(): ${(1..cols).toList().toTypedArray()}
        """)
        for (col in 1..cols) {
            println("col: ${col}")
            //colArray[col] = (1..rows).toList().toTypedArray()
            val colArray = (1..cols).toList().toTypedArray()
            for (row in 1..rows) {
                println("row: ${row}")
                val rowArray = (1..cols).toList().toTypedArray()
                result.add(arrayOf(col,row))
            }
        }

        println(""""
            result: ${Arrays.toString(result)}
            result.size: ${result.size}
        """)
        // return colArray
    }
}

fun main() {
    // println("Welcome to Game of Life, now I need to create the game")

    val cols = 10
    val rows = 10

    val gameOfLife = GameOfLife()
    val grid = gameOfLife.makeArray(cols,rows)
    println("grid: ${grid}")

    // for (let col = 0; col < cols; col++) {
    //     for (let row = 0; row < rows; row++) {

    //     }
    // }
}
